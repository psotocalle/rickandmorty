//import react
import React from "react";

//import devextreme utilities
import { TreeList, SearchPanel, Column,} from "devextreme-react/tree-list";




class App extends React.Component {
  render() {
    return (

      // creo treeList importado de devextreme para el manejo de los datos adquiridos del .json
      <TreeList
        id="id"

        // defino de donde adquiero los datos con los que se trabajara
        dataSource={"json/rickandmorty.json"}
        columnAutoWidth={true}
        wordWrapEnabled={true}
        showBorders={true}
        keyExpr="id"

        
      >
 
        <SearchPanel visible={true} />

        
        {/*  defino y nombro las columnas a mostrar en el panel de busqueda */}
        <Column minWidth={10} dataField="id" />

        <Column minWidth={100} dataField="name" />

        <Column minWidth={120} dataField="status" />

        <Column dataField="species" />

        <Column dataField="gender" />

        <Column dataField="origin" />

        <Column dataField="created" />

        <Column dataField="image" />

      </TreeList>
    );
  }
}

export default App;
