import React from 'react';
import ReactDOM from 'react-dom';

//import archive: app.js
import App from './App.js';


ReactDOM.render( <App />, document.getElementById('app'));
